package com.example.lab4hands_onassignment3;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView tv_popup;
    private TextView tv_result;
    private EditText et_number1;
    private EditText et_number2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv_popup = findViewById(R.id.tv_popup);
        tv_result = findViewById(R.id.tv_result);
        et_number1 = findViewById(R.id.et_number1);
        et_number2 = findViewById(R.id.et_number2);
        registerForContextMenu(tv_popup);
        registerForContextMenu(tv_result);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_layout, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.option1:
                Toast.makeText(this, "option1", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.option2:
                Toast.makeText(this, "option2", Toast.LENGTH_SHORT).show();
                return true;
                case R.id.add:
               addNumbers();
                return true;
                case R.id.sub:
                    subNumbers();
                return true;
                case R.id.mul:
                    mulNumbers();
                return true;
                case R.id.div:
                    divNumbers();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @SuppressLint("SetTextI18n")
    private void addNumbers(){
       if( validateInputs()){
        tv_result.setText((Double.parseDouble(et_number1.getText().toString())+Double.parseDouble(et_number2.getText().toString()))+"");
       }
    } private void subNumbers(){
        if( validateInputs()){
            tv_result.setText((Double.parseDouble(et_number1.getText().toString())-Double.parseDouble(et_number2.getText().toString()))+"");
        }
    } private void mulNumbers(){
        if( validateInputs()){
            tv_result.setText((Double.parseDouble(et_number1.getText().toString())*Double.parseDouble(et_number2.getText().toString()))+"");
        }
    } private void divNumbers(){
        if( validateInputs()){
            tv_result.setText((Double.parseDouble(et_number1.getText().toString())/Double.parseDouble(et_number2.getText().toString()))+"");
        }
    }
   Boolean validateInputs(){
       if(et_number1.getText().toString().isEmpty() ||et_number2.getText().toString().isEmpty()){
           Toast.makeText(this, "Fill all fields", Toast.LENGTH_SHORT).show();
           return  false;
       }
       return  true;
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_layout, menu);
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.option1:
            Toast.makeText(this, "option1", Toast.LENGTH_SHORT).show();
            return true;
        case R.id.option2:
            Toast.makeText(this, "option2", Toast.LENGTH_SHORT).show();
            return true;
        case R.id.add:
            addNumbers();
            return true;
        case R.id.sub:
            subNumbers();
            return true;
        case R.id.mul:
            mulNumbers();
            return true;
        case R.id.div:
            divNumbers();
            return true;
        default:
            return super.onOptionsItemSelected(item);
    }    }
}
